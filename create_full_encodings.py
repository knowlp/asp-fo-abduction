#!/usr/bin/env python2
#
# First Order Horn Abduction in Answer Set Programming
# Copyright (C) 2015-2018  Peter Schueller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This script generates all experimental encodings (modules plus KBs)
# in the directory ./encodings/full/
#
# These `full' encodings can be executed using ./engine/run.py with instances
# in ./encodings/instances/ to reproduce experimental results
#
# Note that some encodings in distinct files will have
# the same content, as the same configurations are used
# in multiple tables.
# These can, e.g., be found by running:
# $ md5sum encodings/full/* |sort |less

OUTDIR='./encodings/full/'
ENCODINGDIR='./encodings/'

experiments = {}

always = ['modules/base/projection.lp', 'kb/sortnames.lp']
globalconstraints = ['kb/global-constraints.lp']

kbs = {
  'bwd-uf': 'kb/bwd-uf-skolem.lp',
  'bwd-flex': 'kb/bwd-flex-skolem.lp',
  'fwd-uf': 'kb/fwd-flex-skolem.lp',
  'fwd-flex': 'kb/fwd-uf-skolem.lp',
}
bases = {
  'bwd-g': 'modules/base/bwd-g.lp',
  'bwd-ai': 'modules/base/bwd-ai.lp',
  'bwd-a': 'modules/base/bwd-a.lp',
  'fwd-a': 'modules/base/fwd-a.lp',
}
objectives = {
  'card': 'modules/obj/card.lp',
  'coh': 'modules/obj/coh.lp',
  'wa': 'modules/obj/wa.lp',
}
acyctrans = {
  'below-rules': 'modules/acyc-trans/below.lp',
  'eq-rules': 'modules/acyc-trans/eq.lp',
  'below-ondemand': 'modules/acyc-trans/below-ondemand.lp',
  'eq-ondemand': 'modules/acyc-trans/eq-ondemand.lp',
}
flexskolem = {
  'p1': 'modules/skolem/flexskolem-p1.lp',
  'p2': 'modules/skolem/flexskolem-p2.lp',
  'g1': 'modules/skolem/flexskolem-g1.lp',
  'g2': 'modules/skolem/flexskolem-g2.lp',
  'infty': 'modules/skolem/flexskolem-infty.lp'
}

def concatToFile(out, inputs):
  ofname = OUTDIR+out
  print "writing file "+ofname
  with file(ofname, 'w+') as of:
    for i in inputs:
      of.write('%\n% == {} ==\n%\n\n'.format(i))
      with file(ENCODINGDIR+i, 'r') as inf:
        of.write(inf.read())
      of.write('\n')

#
# Creating Encodings for Fundamenta Informaticae (see Readme.md [1])
#

# Table 2
for bn, bf in bases.items():
  for on, of in objectives.items():
    if bn.startswith('fwd') and on != 'card':
      continue
    kb = None
    if bn.startswith('bwd'):
      kb = kbs['bwd-uf']
    else:
      kb = kbs['fwd-uf']
    acycs = None
    if bn.endswith('-a'):
      acycs = [acyctrans['eq-rules']]
    else:
      acycs = [acyctrans['eq-rules'], acyctrans['below-rules']]
    concatToFile('fi-table2-pure-asp-{}-{}.lp'.format(bn, on),
      [bf, of] + acycs + always + [kb] + globalconstraints)

# Table 3
for bn, bf in bases.items():
  for on, of in objectives.items():
    if bn.startswith('fwd') and on != 'card':
      continue
    for sk in ['uf', 'flex']:
      kb = None
      if bn.startswith('bwd'):
        kb = kbs['bwd-{}'.format(sk)]
      else:
        kb = kbs['fwd-{}'.format(sk)]
      acycs = None
      if bn.endswith('-a'):
        acycs = [acyctrans['eq-rules']]
      else:
        acycs = [acyctrans['eq-rules'], acyctrans['below-rules']]
      flexconfig = []
      if sk == 'flex':
        flexconfig = [flexskolem['infty']]
      concatToFile('fi-table3-skolemization-{}-{}-{}.lp'.format(sk, bn, on),
        [bf, of] + acycs + flexconfig + always + [kb] + globalconstraints)

# Table 4
for bn, bf in bases.items():
  if bn == 'bwd-g':
    continue
  for on, of in objectives.items():
    if bn.startswith('fwd') and on != 'card':
      continue
    for acyc in ['rules', 'ondemand']:
      kb = None
      if bn.startswith('bwd'):
        kb = kbs['bwd-uf']
      else:
        kb = kbs['fwd-uf']
      acycs = None
      if bn.endswith('-a'):
        acycs = [acyctrans['eq-{}'.format(acyc)]]
      else:
        acycs = [acyctrans['eq-{}'.format(acyc)], acyctrans['below-{}'.format(acyc)]]
      concatToFile('fi-table4-acyctrans-{}-{}-{}.lp'.format(acyc, bn, on),
        [bf, of] + acycs + flexconfig + always + [kb] + globalconstraints)

# Table 5
for on, of in objectives.items():
  for sn, sf in flexskolem.items():
    bf = bases['bwd-a']
    kb = kbs['bwd-flex']
    acycs = [acyctrans['eq-rules']]
    concatToFile('fi-table5-skolemlimit-{}-bwd-a-{}.lp'.format(sn, on),
      [bf, of] + acycs + [sf] + always + [kb] + globalconstraints)

# Table 6
for bn, bf in bases.items():
  if bn == 'bwd-g':
    continue
  for on, of in objectives.items():
    if bn.startswith('fwd') and on != 'card':
      continue
    for globalc in ['present', 'omitted']:
      kb = None
      if bn.startswith('bwd'):
        kb = kbs['bwd-uf']
      else:
        kb = kbs['fwd-uf']
      acycs = None
      if bn.endswith('-a'):
        acycs = [acyctrans['eq-rules']]
      else:
        acycs = [acyctrans['eq-rules'], acyctrans['below-rules']]
      globalcs = []
      if globalc == 'present':
        globalcs = globalconstraints
      concatToFile('fi-table6-globalconstraints-{}-{}-{}.lp'.format(globalc, bn, on),
        [bf, of] + acycs + always + [kb] + globalcs)

#
# Creating Encodings for RCRA 2016 (see Readme.md [2])
#

for bn, bf in bases.items():
  if bn != 'bwd-a':
    continue
  for on, of in objectives.items():
    for acyc in ['rules', 'ondemand']:
      kb = kbs['bwd-uf']
      flexconfig = []
      acycs = []
      if acyc == 'rules':
        acycs.append(acyctrans['eq-rules'])
      concatToFile('rcra16-{}-{}-{}.lp'.format(acyc, bn, on),
        [bf, of, kbs['bwd-uf']] + acycs + always + globalconstraints)
