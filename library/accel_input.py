#!/usr/bin/python

# First Order Horn Abduction in Answer Set Programming
# Copyright (C) 2015  Peter Schueller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# Read ACCEL KB and instances from its lisp representation into inputformat.py representation.
#

# python dependencies
import re
import sys

# local dependencies
import lispparser
from inputformat import Instance, KB, Builtin, RuleBase, Rule, Constraint

def warn(*args):
  sys.stderr.write(' '.join(map(str,args))+'\n')

# we want constants to look like this (as in ASP)
rCONST = re.compile(r'^[a-z_][a-zA-Z0-9_]*$')
def const(sth):
  sth = sth.replace('-','_')
  if not sth[0].islower():
    sth = '_'+sth
  if not rCONST.match(sth):
    raise Exception("could not convert {} to const!".format(sth))
  return sth

# we want variables to look like this (as in ASP)
rVAR = re.compile(r'^[A-Z][a-zA-Z0-9_]*$')
def var(sth):
  if sth[0] == '?':
    sth = sth[1].upper() + sth[2:]
  if not rVAR.match(sth):
    raise Exception("could not convert {} to var!".format(sth))
  return sth

def convert(sth):
  if sth[0] == '?':
    return var(sth)
  else:
    return const(sth)

class AccelParser:
  def parseInstances(self, fname):
    with open(fname, 'r') as f:
      content = f.read()
      return self.parseInstancesFromString(content)

  def parseInstancesFromString(self, string):
    ret = {}
    r = lispparser.parse(string)
    last = { 'nbr': None, 'a': None, 'e': None }
    for itm in r:
      if not isinstance(itm, list):
        continue
      if len(itm) != 3:
        continue
      if itm[0] != 'setf' or itm[1][0] != '*':
        continue
      #warn('seeing item '+'/'.join(itm[0:2]))
      ident = itm[1][1:-1] # remove stars to get something like a1 or e3
      if ident[-1] == 'b':
        continue # "old solutions"
      if ident[0] == 't':
        # this is a hack because testing and training examples come in different naming schemes
        ident = 'e'+ident
      # we hope (and assert) that they come in pairs
      #warn("last nbr {} ident {}".format(last['nbr'], ident))
      assert(last['nbr'] is None or last['nbr'] == ident[1:])
      quotedlist = itm[2]
      assert(quotedlist[0] == "'")
      atoms = quotedlist[1]
      # store what we got
      last[ident[0]] = atoms
      last['nbr'] = ident[1:]
      if last['e'] and last['a']:
        name = last['nbr']
        goal = map(lambda elem: tuple(map(convert, elem)), last['e'])
        solution = map(lambda elem: tuple(map(convert, elem)), last['a'])
        ret[name] = Instance(name, goal, solution)
        last['nbr'] = last['a'] = last['e'] = None
    return ret

  def parseKB(self, fname):
    with open(fname, 'r') as f:
      content = f.read()
      return self.parseKBFromString(content)

  def parseKBFromString(self, string):
    ret = KB()
    r = lispparser.parse(string)
    ret.axioms = self.parseBRules(r)
    sortnames, axioms = self.parseSorts(r)
    ret.sortnames = sortnames
    ret.axioms += axioms
    plannames, axioms = self.parsePlanSteps(r)
    ret.plannames = plannames
    ret.axioms += axioms
    ret.assumptionconstraints = self.parseAssumptionNogoods(r)
    ret.constraints = self.parseUniqueSlotPredicates(r) + self.parseNogoods(r)
    return ret

  def parseBRules(self, r):
    ret = []
    relevant = filter(lambda x: x[0] == 'setf' and x[1] == '*brules*', r)
    for brules in relevant:
      quotedlist = brules[2]
      assert(quotedlist[0] == "'")
      quotedlist = quotedlist[1]
      for rule in quotedlist:
        #print 'rule '+repr(rule)
        head = tuple(map(convert, rule[1]))
        body = map(lambda elem: tuple(map(convert, elem)), rule[2:])
        ret.append(Rule(head, body))
    return ret

  def parseSorts(self, r):
    sorts = set()
    ret = []
    relevant = filter(lambda x: x[0] == 'setf' and x[1] == '*sort-hierarchy*', r)
    for brules in relevant:
      quotedlist = brules[2]
      assert(quotedlist[0] == "'")
      quotedlist = quotedlist[1]
      for sortdecl in quotedlist:
        #print 'sortdecl '+repr(sortdecl)
        assert(sortdecl[1] == '.')
        supersort = sortdecl[0]
        sorts.add(convert(supersort))
        for subsort in sortdecl[2]:
          sorts.add(convert(subsort))
          head = ('inst','X',convert(supersort))
          body = [('inst','X',convert(subsort))]
          ret.append(Rule(head, body))
    return sorts, ret

  def parsePlanSteps(self, r):
    ret = []
    plannames = set()
    relevant = filter(lambda x: x[0] == 'setf' and x[1] == '*plan-steps*', r)
    for brules in relevant:
      quotedlist = brules[2]
      assert(quotedlist[0] == "'")
      quotedlist = quotedlist[1]
      for decl in quotedlist:
        #print 'decl '+repr(decl)
        assert(decl[1] == '.')
        highlevel_plan = decl[0]
        steps = decl[2]

        # every plan declaration becomes one huge axiom
        planvar = 'X'
        stepvars = ['A{}'.format(i) for i in range(0, len(steps))]

        planname = convert(highlevel_plan)
        plannames.add(planname)
        head = ('inst', planvar, planname)
        body = []
        # step in body
        for idx, s in enumerate(steps):
          b = (convert(s), planvar, stepvars[idx])
          body.append(b)
        # precede in body
        # this exists in some papers but not in the ACCEL DB!
        #for idx in range(1, len(steps)):
        #  b = ('precede', stepvars[idx-1], stepvars[idx])
        #  body.append(b)
        ret.append(Rule(head, body))
    return plannames, ret

  def parseAssumptionNogoods(self, r):
    ret = []
    relevant = filter(lambda x: x[0] == 'setf' and x[1] == '*assumption-nogoods*', r)
    #warn('relevant '+repr(relevant))
    for nogoods in relevant:
      quotedlist = nogoods[2]
      assert(quotedlist[0] == "'")
      lst = quotedlist[1]
      for nogood in lst:
        #warn('nogood '+repr(nogood))
        body = map(lambda elem: tuple(map(convert, elem)), nogood)
        ret.append(Constraint(body))
    return ret

  def parseNogoods(self, r):
    ret = []
    relevant = filter(lambda x: x[0] == 'setf' and x[1] == '*nogoods*', r)
    #warn('relevant '+repr(relevant))
    for nogoods in relevant:
      quotedlist = nogoods[2]
      if quotedlist == 'nil':
        break
      assert(quotedlist[0] == "'")
      lst = quotedlist[1]
      for nogood in lst:
        #warn('nogood '+repr(nogood))
        body = map(lambda elem: tuple(map(convert, elem)), nogood)
        ret.append(Constraint(body))
    return ret

  def parseUniqueSlotPredicates(self, r):
    def makeNogood(pred):
      v1 = (pred, 'E', 'V1')
      v2 = (pred, 'E', 'V2')
      lt = (Builtin('!='), 'V1', 'V2')
      return Constraint([v1, v2, lt])
    relevant = filter(lambda x: x[0] == 'setf' and
      x[1] == '*unique-slot-value-predicates*', r)
    assert(len(relevant) == 1 and isinstance(relevant[0], list))
    quotedlist = relevant[0][2]
    assert(quotedlist[0] == "'")
    quotedlist = quotedlist[1]
    return map(makeNogood, map(convert, quotedlist))

def main():
  p = AccelParser()
  insts = p.parseInstances('accel_STORY/data-pr-train.lisp')
  insts.update(p.parseInstances('accel_STORY/data-pr-test.lisp'))
  warn('instances')
  warn('\n'.join(map(str, insts.itervalues())))
  kb = p.parseKB('accel_STORY/kb-pr.lisp')
  warn('kb')
  warn(kb)

  gmin, gmax, gsum, gcount = 1000, 0, 0, 0
  for name, inst in insts.iteritems():
    g = len(inst.goal)
    gmin = min(gmin, g)
    gmax = max(gmax, g)
    gsum += g
    gcount += 1
  amin, amax, asum, acount = 1000, 0, 0, 0
  for axiom in kb.axioms:
    a = len(axiom.body)
    amin = min(amin, a)
    amax = max(amax, a)
    asum += a
    acount += 1
  print ("stats: goal length: min={} max={} avg={:.1f} count={}".format(gmin, gmax, gsum/float(gcount), gcount) +
         " axiom length: min={} max={} avg={:.1f} count={}".format(amin, amax, asum/float(acount), acount))

if __name__ == '__main__':
  main()
