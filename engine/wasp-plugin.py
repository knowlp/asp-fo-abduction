# First Order Horn Abduction in Answer Set Programming
# Copyright (C) 2016  Peter Schueller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# To execute this plugin you need WASP 2.0 plugins branch
# https://github.com/alviano/wasp/tree/plugins
# (this file worked with c18ca254 from 2016-09-02)

import sys, traceback, atexit

print "Loaded wasp answer set checker plugin!"

eqondemand_id = None

# key = ID, value = (atom1,atom2)
id2EqPair = {}
# key = (atom1,atom2), value = ID
eqPair2Id = {}

# key = ID, value = variable
id2name = {}

# for recording statistics
stats = {
  'invalidated': 0,
  'reasons': 0
}

def printstats():
  global stats
  sys.stderr.write("STATS "+' '.join(map(lambda x: '{}={}'.format(x[0],x[1]), stats.items()))+"\n")
atexit.register(printstats)

# for communication between plugin callbacks
reasons = [0]

def findEndOfTerm(string, start_idx):
  'returns index of following (non-bracketed) "," or ")"'
  open_brck = 0
  for end_idx in range(start_idx,len(string)):
    if string[end_idx] == ')':
      open_brck -= 1
      if open_brck == -1:
        return end_idx
    elif string[end_idx] == '(':
      open_brck += 1
    elif string[end_idx] == ',':
      if open_brck == 0:
        return end_idx
  return len(string)

def addedVarName(var, name):
  global eqondemand_id, id2EqPair, eqPair2Id, id2name
  id2name[var] = name
  if name[:3] != 'eq(':
    if name == 'eqondemand':
      eqondemand_id = var
    return
  #print 'got var', name
  try:
    # pattern: 'eq( ... , ... )'
    first_end = findEndOfTerm(name, 3)
    first = name[3:first_end]
    second = name[first_end+1:-1]
    #print 'split into "{}" and "{}"'.format(first, second)
    id2EqPair[var] = (first, second)
    eqPair2Id[(first, second)] = var
  except:
    traceback.print_exc()

def getVariablesToFreeze():
  global id2EqPair
  return id2EqPair.keys()

def sign(x):
  if x < 0:
    return '-'
  else:
    return ''

def checkAnswerSet(*answerset):
  global eqondemand_id, reasons, id2EqPair, eqPair2Id, stats

  # return 1 straight away if we do not do on-demand checking
  #if eqondemand_id == None or answerset[eqondemand_id] == -eqondemand_id:
  #  return 1

  reasons = [0]
  try:
    #print 'got answer set', repr(answerset)
    # implicit: x > 0 (we only need positive ones to check)
    relevant = filter(lambda x: x in id2EqPair, answerset)
    #print 'relevant:', repr(relevant)
    #print 'relevant mapped:', ', '.join(map(lambda x: '{}/{}'.format(*id2EqPair[x]), relevant))
    eq = {}
    for a, b in map(lambda x: id2EqPair[x], relevant):
      if a in eq:
        eq[a].add(b)
      else:
        eq[a] = set([b])

    good_answerset = 1
    for a, aeq in eq.iteritems():
      for b in aeq:
        # we go over all eq(a,b)
        if b in eq:
          for c in eq[b]:
            # here we go over all eq(b,c)
            # so we have all a, b, c where eq(a,b) and eq(b,c) # and a < b < c
            if c not in aeq:
              #sys.stderr.write("transitivity violation! {} {} {}\n".format(a, b, c))
              good_answerset = 0
              odc = [ -eqPair2Id[(a,b)], -eqPair2Id[(b,c)], eqPair2Id[(a,c)] ]
              reasons += odc + [0]
              stats['reasons'] += 1
              #return good_answerset # TODO this is only if we cannot give more than one reason
    if good_answerset == 1:
      sys.stderr.write("good answer set!\n")
    else:
      stats['invalidated'] += 1
    return good_answerset
  except:
    traceback.print_exc()

def prettify(id_):
  global id2name
  if id_ < 0:
    return '-'+id2name[-id_]
  else:
    return id2name[id_]

def getReasonsForCheckFailure():
  global reasons
  # list of reasons is returned as one list separated by 0 (and terminated by 0)
  #sys.stderr.write('returning reason {}\n'.format(repr(reason)))
  #sys.stderr.write('returning reason pretty {}\n'.format(map(prettify, reason)))
  return reasons

# existence of this method is a flag
def storeClauseFromCheckFailure():
  pass

