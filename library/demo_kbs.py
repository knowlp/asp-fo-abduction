#!/usr/bin/python

# First Order Horn Abduction in Answer Set Programming
# Copyright (C) 2015, 2016  Peter Schueller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# Knowledge Bases for running examples in paper.
#

import inputformat
import accel_input

# this is the running example as submitted to RCRA
def demoInstKbRCRASubmitted(root):
  insts = {}
  goal = [('name', 'm', 'mary'), ('has', 'm', 'h'), ('inst', 'h', 'heart_attack'), ('name', 'j', 'john'), ('inst', 'j', 'depressed')]
  solution = []
  insts['dha'] = inputformat.Instance('dha', goal, solution)
  kb = inputformat.KB()
  kb.axioms.append(inputformat.Rule(('inst', 'X', 'depressed'),
    [('like', 'X', 'Y'), ('inst', 'Y', 'bad_condition'), ('inst', 'Y', 'irreplacable')]))
  kb.axioms.append(inputformat.Rule(('inst', 'X', 'depressed'),
    [('inst', 'X', 'pessimist')]))
  kb.axioms.append(inputformat.Rule(('inst', 'X', 'bad_condition'),
    [('has', 'X','Y'), ('inst', 'Y', 'illness')]))
  kb.axioms.append(inputformat.Rule(('inst','X', 'illness'),
    [('inst', 'X', 'heart_attack')]))
  return insts, kb

# this is the running example published in RCRA Workshop and submitted to FI
# it requires weakening of UNA and value invention
def demoInstKbRCRA(root):
  insts = {}
  goal = [
    ('name', 'm', 'mary'), ('lost', 'm', 'f'), ('father_of', 'f', 'm'),
    ('inst', 's', 'female'), ('is', 's', 'depressed')]
  solution = []
  insts['mlf'] = inputformat.Instance('mlf', goal, solution)
  p = accel_input.AccelParser()
  kb = p.parseKBFromString('''
(setf *brules*
  '(
    (<- (inst ?x male) (father-of ?x ?y))
    (<- (inst ?x female) (name ?x mary))
    (<- (is ?x depressed) (inst ?x pessimist))
    (<- (is ?x depressed) (important-for ?y ?x) (is ?y dead))
    (<- (important-for ?y ?x) (father-of ?y ?x))
    (<- (lost ?x ?y) (inst ?y person) (important-for ?y ?x) (is ?y dead))
   ))

(setf *nogoods*
  '(
    ((inst ?x male) (inst ?x female))
    ((father-of ?x ?y) (inst ?x female))
   ))

(setf *sort-hierarchy*
  '(
    (person . (male female))
    (state . (dead depressed))
   ))

(setf *unique-slot-value-predicates*
  '(go-step goer dest-go source-go vehicle
   ))
  ''')
  return insts, kb
