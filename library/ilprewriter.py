#!/usr/bin/python

# First Order Horn Abduction in Answer Set Programming
# Copyright (C) 2015  Peter Schueller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Converter from ACCEL to input format of Phillip (see runphil.py)
#

import sys

# local libraries
import inputformat

accel_root = './accel_STORY'

def warn(*args):
  sys.stderr.write(' '.join(map(str,args))+'\n')

class SortnameAwareConverter:
  def __init__(self, sortnames):
    self.sortnames = set(sortnames)

  def __call__(self, i):
    if isinstance(i, str):
      # convert one constant
      if i in self.sortnames:
        return i.upper()
      else:
        return i
    elif isinstance(i, tuple):
      return '({})'.format(
        ' '.join([i[0]] + map(self.__call__, i[1:]) ) )

class AllConstConverter:
  def __init__(self):
    pass

  def __call__(self, i):
    if isinstance(i, str):
      # convert one constant
      return i.upper()
    elif isinstance(i, tuple):
      return '({})'.format(
        ' '.join([i[0]] + map(self.__call__, i[1:]) ) )

class ILPRewriter:
  def __init__(self, excludeCyclicAxioms=True):
    self.excludeCyclicAxioms = excludeCyclicAxioms

  def rewriteGoal(self, kb, inst):
    assert(isinstance(inst, inputformat.Instance)) 
    conv = SortnameAwareConverter(kb.sortnames) # more realistic case
    #conv = AllConstConverter() # as done for Henry-n700
    facts = map(conv, inst.goal)
    scosts = ' '.join(['100' for f in facts])
    sfacts = ' '.join(facts)
    return ['(O (^ {} ) (cost {}) )'.format(sfacts, scosts)]

  def rewriteAxioms(self, kb):
    assert(isinstance(kb, inputformat.KB)) 
    conv = SortnameAwareConverter(kb.sortnames)
    out = []
    for rule in kb.axioms:
      # exclude cyclic axioms (this code works for ACCEL but not in general)
      if self.excludeCyclicAxioms:
        isCyclic = False
        hpred = rule.head[0]
        for b in rule.body:
          bpred = b[0]
          if hpred == bpred and rule.head[1].isupper() and rule.head[2].isupper():
            isCyclid = True
        if isCyclic:
          # skip this axiom
          warn("skipped converting axiom '{}' due to cyclicity".format(rule))
          continue
      c = 1.2 / len(rule.body)
      # make everything lower (get rid of uppercase variables)
      tolower = lambda y: tuple(map(lambda x: x.lower(), y))
      body_lowered = map(tolower, rule.body)
      # define function that converts single literals
      mapatom = lambda x: '({pred} {rest} :{cost})'.format(
        cost=c, pred=x[0],
        rest=' '.join(map(conv, x[1:])))
      sbodyatoms = map(mapatom, body_lowered)
      srule = '(B (=> (^ {} ) {}) )'.format(' '.join(sbodyatoms), conv(tolower(rule.head)))
      out.append(srule)
    return out

  def rewriteConstraints(self, kb):
    # we can handle a limited set of cases with this code
    #
    # UNIQUE-SLOT-constraints:
    #   <- token(E,V1), token(E,V2), V1 != V2
    #
    # from the documentation, the following eliminates cases where both are true:
    # (B (xor (cat x) (dog x)))
    #
    # hence we rewrite into
    # (B (xor (token e v1) (token e v2)))
    #
    assert(isinstance(kb, inputformat.KB)) 
    conv = SortnameAwareConverter(kb.sortnames)
    out = []
    for constraint in kb.constraints:
      if len(constraint.body) == 3:
        # UNIQUE-SLOT

        # check that 1 and 2 are not builtins and that 3 is !=
        for b in constraint.body[0:2]:
          if isinstance(b[0], inputformat.Builtin):
            raise Exception("TODO: implement builtin for 3-body-constraints")
        if (not isinstance(constraint.body[2][0], inputformat.Builtin)) or constraint.body[2][0].operator != '!=':
          raise Exception("TODO: implement other builtin for 3-body-constraints {}".format(str(constraint)))

        pred = constraint.body[0][0]
        assert(pred == constraint.body[1][0]) # same pred for unique slot
        var = constraint.body[0][1]
        assert(var == constraint.body[1][1]) # same var in first place
        # let's assume the rest is ok

        # henry
        #template = '(B (_|_ ({pred} {var} {v1}) ({pred} {var} {v2})))'
        # phillip
        template = '(B (xor ({pred} {var} {v1}) ({pred} {var} {v2})))'
        out.append(template.format(
          pred=pred, var=var.lower(),
          v1=constraint.body[0][2].lower(),
          v2=constraint.body[1][2].lower()))
      else:
        raise Exception("unknown constraint {}".format(constraint))
    return out

  def rewriteKB(self, kb):
    out = []
    out += self.rewriteAxioms(kb)
    #out += self.rewriteConstraints(kb) # make it easier for Phillip
    return out

if __name__ == '__main__':
  import accel_input
  p = accel_input.AccelParser()
  print "using '{}' as root directory for the ACCEL lisp files".format(accel_root)
  insts = p.parseInstances(accel_root+'/data-pr-train.lisp')
  kb = p.parseKB(accel_root+'/kb-pr.lisp')
  conv = ILPRewriter()
  outpref = 'ilp'
  inst = '17'
  print "storing instance {} with prefix {}".format(inst, outpref)
  with open(outpref+'-inst.lisp', 'w+') as f:
    f.write('\n'.join(conv.rewriteGoal(kb, insts[inst]))+'\n')
  with open(outpref+'-kb.lisp', 'w+') as f:
    f.write('\n'.join(conv.rewriteKB(kb))+'\n')
