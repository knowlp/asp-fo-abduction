# First Order Horn Abduction in Answer Set Programming
# Copyright (C) 2015  Peter Schueller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# For the "gringo" import, you need
# * download clingo 4.5.0 source from potassco
#   http://sourceforge.net/projects/potassco/files/clingo/4.5.0/clingo-4.5.0-source.tar.gz/download
# * configure and build it with target pyclingo:
#   $ scons --build-dir=release pyclingo
# * put the path to gringo.so into PYTHONPATH
import gringo

import sys
import json
import re
import collections
import time
import networkx as nx # for detecting graph cycles

def warn(msg):
  sys.stderr.write(msg+'\n')

#
# START OF EXTERNAL ATOM IMPLEMENTATION FOR SKOLEMIZATION
#

# GLOBALS FOR SKOLEMIZATION CONFIGURATION
skolem_limit = 'infinite'
skolem_limit_type = 'generation'

# GLOBALS FOR SKOLEMIZATION BOOKKEEPING
# skolem limiting:
# * skolem_limit(generation,[0-9]+)
#   skolem term -> { generation, instance }
#   (0 = created without skolem-term generated in this skolem instance)
#   (1 = created with at least one skolem-term of generation 0 generated in this skolem instance, disallowed in ILP solution)
#   (2 = created with at least one skolem-term of generation 1 generated in this skolem instance)
#   the skolem instance is an integer given as last argument of @skolem
#   this integer is counted up for each invented value in each nonground rule
# * skolem_limit(parent,[0-9]+)
#   skolem term -> { generation }
#   (0 = created without skolem-term)
#   (1 = created with at least one skolem-term of generation 0)
#   (2 = created with at least one skolem-term of generation 1)
# terms -> skolem term
skolem_registry = {}
# next free index
skolem_index = 0
# bookkeeping of generations
skolem_info = {}
# statistics
skolem_skipped = 0
def skolem(*terms):
  global skolem_registry
  global skolem_index
  global skolem_limit
  global skolem_limit_type
  global skolem_skipped
  #print ">> skolem got terms {}".format(repr(terms))
  if terms not in skolem_registry:
    if skolem_limit == 'infinite':
      # the original case: infinite skolemization
      newterm = gringo.Fun('p'+str(skolem_index))
      skolem_index += 1
      skolem_registry[terms] = newterm
      return newterm
    else:
      # generation-limited skolemization
      inst = terms[-1]
      assert(isinstance(inst, int)) # we need a running integer to distinguish skolem functions
      # determine generation of new term
      gen = 0
      if skolem_limit_type == 'generation':
        for t in terms[:-1]:
          if t in skolem_info and skolem_info[t]['inst'] == inst:
            gen = max(gen, skolem_info[t]['gen']+1)
      elif skolem_limit_type == 'parent':
        for t in terms[:-1]:
          if t in skolem_info:
            gen = max(gen, skolem_info[t]['gen']+1)
      if gen >= skolem_limit:
        # will not skolemize so far into the generation depth
        #warn('@skolem({}) for inst {} found gen {} for limit {} not skolemizing!'.format(
        #  repr(terms), repr(inst), repr(gen), repr(skolem_limit)))
        skolem_skipped += 1 # record how often we did not skolemize
        return []
      newterm = gringo.Fun('p'+str(skolem_index))
      skolem_index += 1
      skolem_registry[terms] = newterm
      skolem_info[newterm] = { 'inst': inst, 'gen': gen }
      #warn('@skolem({}) for inst {} found gen {} for limit {} returning newterm {}'.format(
      #  repr(terms), repr(inst), repr(gen), repr(skolem_limit), repr(newterm)))
      return newterm
  else:
    return skolem_registry[terms]

#
# START OF IMPLEMENTATION FOR ON-DEMAND CONSTRAINT
#

on_demand_constraints = []
on_demand_constraint_counter = 0
def propagateOnDemand(model):
  '''
  returns False if the model violates a constraint
  '''
  global on_demand_constraint_counter
  global on_demand_constraints
  global prg

  invalidate = False

  # on demand unique slot/assumption constraints
  odu = []
  oda = []
  for atm in model.atoms(gringo.Model.ATOMS):
    if atm.name() == 'ondemanduniqueslot':
      odu.append( atm.args() )
    elif atm.name() == 'ondemandassumption':
      oda.append( atm.args() )

  eq = {}
  if model.contains(gringo.Fun('eqondemand')) or len(odu)+len(oda) > 0:
    # collect equivalence classes
    for atm in model.atoms(gringo.Model.ATOMS):
      if atm.name() != 'eq':
        continue
      a, b = atm.args()
      # only process a < b (reflexivity and symmetry is enforced in rules)
      # skip symmetry breaking for now
      #if gringo.cmp(a, b) > -1:
      #  continue
      # integrate into eqc
      if a in eq:
        eq[a].add(b)
      else:
        eq[a] = set([b])

  if model.contains(gringo.Fun('eqondemand')):
    # check eq/2 predicate for equivalence relation on demand
    # (check transitivity)
    for a, aeq in eq.iteritems():
      for b in aeq:
        # we go over all eq(a,b) #where a < b
        if b in eq:
          for c in eq[b]:
            # here we go over all eq(b,c) #where b < c
            # so we have all a, b, c where eq(a,b) and eq(b,c) # and a < b < c
            if c not in aeq:
              #sys.stderr.write("transitivity violation! {} {} {}\n".format(a, b, c))
              invalidate = True
              odc = [ (gringo.Fun('eq',(a, b)), True),
                      (gringo.Fun('eq',(b, c)), True),
                      (gringo.Fun('eq', (a, c)), False)]
              model.context.add_nogood(odc)
              on_demand_constraints.append(odc)
              on_demand_constraint_counter += 1
    if not invalidate:
      #sys.stderr.write("no transitivity violation!\n")
      pass

  if model.contains(gringo.Fun('belowondemand')):
    # check below/2 predicate for partial order on demand
    # (forbid cyclicity)

    # represent in directed graph
    g = nx.DiGraph()
    # first get all pairs
    for atm in model.atoms(gringo.Model.ATOMS):
      if atm.name() != 'below':
        continue
      #warn(" processing POS below {}".format(str(atm)))
      a, b = atm.args()
      g.add_edge(str(a), str(b), atm=atm) # store the atom in the edge
    #warn("below POS graph {} {}".format(g.nodes(), g.edges()))

    # find all simple cycles:
    # (forbidding simple cycles is sufficient for forbidding all cycles)
    cycles = list(nx.simple_cycles(g))
    if len(cycles) > 0:
      invalidate = True
      for cycle in cycles:
        #sys.stderr.write("below POS violation! cycle {}\n".format(repr(cycle)))
        odc = []
        # simple_cycles returns lists where first and last element are the same
        for idx in range(0, len(cycle)-1):
          a, b = cycle[idx], cycle[idx+1]
          eatm = g.edge[a][b]['atm'] # get the atom from the edge
          # (recreating the atom with gringo.Fun did not work properly!)
          #warn(" found atom {} for edge {} to {}".format(eatm, a, b))
          odc.append( (eatm, True) )
        sys.stderr.write("  adding below odc {!r}\n".format(repr(odc)))
        model.context.add_nogood(odc)
        on_demand_constraints.append(odc)
        on_demand_constraint_counter += 1

  return not invalidate

#
# CONFIGURATION FOR OPTIMIZATION+ON-DEMAND-CONSTRAINTS
#
def configure(encoding_files):
  '''
  configure global settings from facts in encodings
  (yes, this could be solved conceptually more elegantly)
  (this would probably require inspecting the domain after grounding)
  '''
  source = '\n'.join(map(
    lambda x: open(x, 'r').read(), encoding_files))

  mo = re.match(r'.*^skolem_limit\(([a-z]+),([0-9]+)\)\.$.*', source, re.MULTILINE | re.DOTALL)
  if mo:
    skolem_limit_type = mo.groups()[0]
    skolem_limit = int(mo.groups()[1])

  mo = re.match(r'.*^answerset_limit\(([0-9]+)\)\.$.*', source, re.MULTILINE | re.DOTALL)
  answerset_limit = 1
  if mo:
    answerset_limit = int(mo.groups()[0])

#
# ENGINE FOR OPTIMIZATION+ON-DEMAND-CONSTRAINTS
#

def jsonout(msg, newline=True, initialcomma=True):
  '''
  output part of a JSON array
  '''
  if initialcomma:
    sys.stdout.write(',')
  if newline:
    sys.stdout.write('\n')
  sys.stdout.write(msg)

def outputBest(model):
  '''
  output best model + diagnostics for automatic data analysis
  '''
  global skolem_skipped
  jsonary = '['+','.join(map(lambda x: '"'+str(x)+'"',model['Value']))+']'
  jsonout('{{ "evt": "model", "Value": {}, "Cost": {}, "skolem_skipped": {} }}'.format(
    jsonary, model['Cost'], skolem_skipped))

best_model = {'Value': None, 'Cost': []}
optimistic_cost = None
trying_cost = None
answersets_found = 0
def on_model_optimistic(model):
  global best_model
  global optimistic_cost
  global trying_cost
  global answersets_found
  global answerset_limit

  # first handle on-demand-transitivity of eq
  goodmodel = propagateOnDemand(model)

  # now handle optimization
  # remember best cost of any model we see
  # (for sure the legal model will have this cost or more expensive)
  thiscost = model.optimization()
  if answersets_found < 10:
    warn("{} model: clingo cost {}".format('good' if goodmodel else 'bad', thiscost))
  optimistic_cost = thiscost
  if goodmodel:
    # remember the best legal model we see
    best_model['Value'] = model.atoms(gringo.Model.SHOWN)
    best_model['Cost'] = thiscost
    if answersets_found < 10:
      sys.stderr.write("got good model thiscost {} tryingcost {}\n".format(
        thiscost, trying_cost))
    if trying_cost == thiscost:
      if answersets_found < 10:
        outputBest(best_model)
      answersets_found += 1
      if answerset_limit != 0 and answersets_found >= answerset_limit:
        # we want to terminate the search
        # throwing an exception fails, and there is no API for stopping
        # so we force unsatisfiability by requesting something
        # that never can be true to become true
        constr = [ (gringo.Fun('donald_duck'), False) ]
        model.context.add_nogood(constr)

answerset_limit = 1
def doOptimisticOptimization(asp_files, other_args):
  global skolem_index
  global on_demand_constraint_counter
  global best_model
  global optimistic_cost
  global trying_cost
  global prg
  global answerset_limit
  global answersets_found

  sys.stderr.write("doOptimisticOptimization\n")

  clingoargs = []
  #clingoargs.append('--verbose')
  #clingoargs.append('--lparse-debug=all')
  for a in other_args:
    clingoargs.append(a)

  # find a bound aggressively
  prg = gringo.Control(clingoargs)
  prg.conf.solve.opt_mode = 'opt'
  for f in asp_files:
    prg.load(f)
  prg.ground([('base', [])])
  prg.solve(on_model=on_model_optimistic)
  jsonout('{{ "evt": "boundfinished", "createdSkolem" : {}, "createdOnDemandConstraint" : {}, "stats":{} }}'.format(
    skolem_index, on_demand_constraint_counter,
    json.dumps(prg.stats, sort_keys=True, indent=2, separators=(',', ': '))))

  
  if best_model['Cost'] == optimistic_cost:
    outputBest(best_model)
    answersets_found = 1
    if answerset_limit == 1:
      return

  # find all models of optimistic_cost
  leave = False
  iteration = 1
  trying_cost = optimistic_cost
  while not leave:
    prg.conf.solve.opt_mode = 'enum'
    prg.conf.solve.opt_bound = ','.join(map(str,trying_cost))
    prg.conf.solve.models = 0 # all models
    prg.solve(on_model=on_model_optimistic)
    jsonout(('{{ "evt": "iteration", "createdSkolem" : {},'+
      ' "createdOnDemandConstraint" : {}, "iteration": {},'+
      ' "trying_cost": {}, "stats":{} }}').format(
      skolem_index, on_demand_constraint_counter, iteration, trying_cost,
      json.dumps(prg.stats, sort_keys=True, indent=2, separators=(',', ': '))))
    if best_model['Cost'] == trying_cost:
      # we found a model of the cost that we are trying to find
      # or we found no model in callback but found 1 model before
      # (the program has only one answer set)
      leave = True
    else:
      # try a worse objective (more cost)
      if len(trying_cost) > 1:
        sys.stderr.write("WARNING: this code is not yet suitable for multi-level-optimization (only first level will be optimized)!\n")
      trying_cost = [trying_cost[0]+1]
    iteration += 1

def main():
  global skolem_limit
  global skolem_limit_type
  global answerset_limit
  if len(sys.argv) < 2:
    raise Exception("need at least one argument! (ASP file to load)")
  asp_files = filter(lambda x: not x.startswith('-'), sys.argv[1:])
  other_args = filter(lambda x: x.startswith('-'), sys.argv[1:])

  jsonout('[', initialcomma=False)
  try:
    jsonout('{ "evt": "startup" }', initialcomma=False)

    # this a hacky thing but fastest to implement at the moment
    configure(asp_files)

    doOptimisticOptimization(asp_files, other_args)

    jsonout('{{ "evt": "end", "answersets_found":{} }}'.format(answersets_found))

  finally:
    jsonout(']', initialcomma=False)

if __name__ == '__main__':
  main()
