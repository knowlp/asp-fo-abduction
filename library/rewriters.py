# First Order Horn Abduction in Answer Set Programming
# Copyright (C) 2015  Peter Schueller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# python libraries
import sys
import collections

# local libraries
import inputformat

def warn(*args):
  sys.stderr.write(' '.join(map(str,args))+'\n')

######################################
## RCRA 2015 experimental rewriters ##
######################################

def isVar(sth):
  return sth[0].isupper()

def rRep(ridx):
  return 'r{}'.format(ridx)

def cli(tup):
  'makes a c(.,.,.) style atom out of a tuple/list'
  assert(len(tup) > 0)
  if len(tup) == 1:
    return tup[0]
  else:
    if isinstance(tup[0], inputformat.Builtin):
      return "{arg1} {op} {arg2}".format(op=str(tup[0]), arg1=tup[1], arg2=tup[2])
    else:
      return "c({})".format(','.join(tup))

class ValueInventer:
  '''
  usage pattern: construct with appropriate setting, then transform
   * 'python' use gringo python externals to create smaller unique terms
   * 'function' use function terms to invent values
   * 'null' does not invent values but puts constant 'null'
  '''

  def __init__(self, invent='function', function_name='s', python_function_name='skolem'):
    assert(invent in ['function', 'python', 'null'])
    self.invent = invent
    self.function_name = function_name
    self.python_function_name = python_function_name

  def do(self, parameters):
    assert(isinstance(parameters, list))
    skolemterm = ''
    if self.invent == 'function':
      # uninterpreted function for value invention
      skolemterm = '{}({})'.format(
        self.function_name, ','.join(parameters))
    elif self.invent == 'python':
      # Gringo external for value invention
      skolemterm = '@{}({})'.format(
        self.python_function_name, ','.join(parameters))
    elif self.invent == 'null':
      skolemterm = 'null'
    return skolemterm

class VariableMapper:
  '''
  usage pattern: construct one instance of this mapper for each rule to process
  '''
  # index to create new skolem terms
  # this index is globally increasing over all rules
  skolemidx = 1

  def __init__(self, invent='function'):
    '''
    '''
    # variable index, to create ASP variables
    # this index is local to instances of the mapper
    # (i.e., local to the currently processed rule)
    self.vidx = 1
    # record non-skolemized terms
    self.nonskolemvars = set()
    # store mapping from variables to skolem terms
    self.mapper = {}
    #warn("VariableMapper initialized with index {}".format(VariableMapper.skolemidx))
    self.inventer = ValueInventer(invent)

  def transform_register(self, terms):
    '''
    register variables
    '''
    #warn("VariableMapper registering terms  {}".format(repr(terms)))
    for idx, term in enumerate(terms):
      if isVar(term):
        self.nonskolemvars.add(term)
    #warn("VariableMapper register result {}".format(repr(terms)))

  def transform_skolemize(self, terms):
    '''
    use mapper to map variables
    skolemize new variables using existing variables
    returns None or string with additional body elements for value invention
    '''
    #warn("VariableMapper skolemizing terms {}".format(repr(terms)))
    #warn("  (skolem_args {})".format(skolem_args))
    #warn("  (hterms {})".format(skolem_hterms))
    out = []
    for idx, term in enumerate(terms):
      if isVar(term) and term not in self.nonskolemvars:
        try:
          # first try
          terms[idx] = self.mapper[term]
        except KeyError:
          # if it does not work: register new skolem term
          #warn("VariableMapper skolemizing with index {}".format(VariableMapper.skolemidx))

          # we have at least one term: the index (to make it unique to the rule)
          skolemparameters = list(self.nonskolemvars) + [str(VariableMapper.skolemidx)]
          self.mapper[term] = self.inventer.do(skolemparameters)
          # again, now it must work!
          terms[idx] = self.mapper[term]

          # globally use this skolem idx
          VariableMapper.skolemidx += 1
    #warn("VariableMapper skolemize result {}".format(repr(terms)))

class CTripleRewriterRCRABase(object):
  '''
  Rewriter for RCRA experiments:
   * using c(pred,subj,obj) representation for atom pred(subj,obj).
   * using map(to,from) for representing equivalences.
  '''
  def __init__(self):
    pass

  def rewrite(self, inst, kb):
    out = []
    out += self.rewriteGoal(inst.goal)
    out += self.rewriteAxioms(kb.axioms)
    out += self.rewriteConstraints(kb.constraints)
    out += self.rewriteAssumptionConstraints(kb.assumptionconstraints)
    out += self.rewriteSorts(kb.sortnames)
    return out

  def rewriteGoal(self, goal):
    out = []
    warn('goal to rewrite: '+repr(goal))
    for b in goal:
      for t in b:
        # assume we have only constants in the goal (as in ACCEL)
        assert(not isVar(t))
      out.append('goal({}).'.format(cli(b)))
    return out

  def rewriteConstraintsHelper(self, constraints, forbidWhat, map_it):
    # forbidWhat is the predicate used to forbid
    # (see rewriteConstraints and rewriteAssumptionConstraints)
    # map_it == True -> require map/2 for matching
    out = []
    for constraint in constraints:
      # we do not use % ... because this would make line breaks relevant
      # this method has the advantage to put the full rules to the answer set
      # the disadvantage is a large symbol table (more than double size)
      out.append('comment("{}").'.format(str(constraint)))
      
      # we do not need any skolemized terms here
      # true atoms are marked with true(<atom>) using mapping map(<to>,<from>)
      # but we need one canonical term for each variable
      # e.g. for   "<- get_straw_step(D,G), agent_get(G,A)" we produce
      # ":- true(c(get_straw_step,D,G1)), true(c(agent_get,G2,A)),
      #     map(G,G1), map(G,G2)"
      # (<forbidWhat> is used instead of 'true')
      # (this example is if map_it is True)
      varusage = collections.defaultdict(lambda: 0)
      for atom in constraint.body:
        for term in atom[1:]:
          if isVar(term):
            varusage[term] += 1
      # now we know which variables must be changed
      # (those that are used more than once)
      #
      # create output atoms and mappings
      usedvars = set(varusage.keys())
      newidx = 0
      thisout = []
      for atom in constraint.body:
        thisatom = [atom[0]]
        for term in atom[1:]:
          if term in varusage and varusage[term] > 1 and map_it:
            # find new variable
            while True:
              newvar = 'X{}'.format(newidx)
              newidx += 1
              if newvar not in usedvars:
                break
            # output mapping
            thisout.append('map({},{})'.format(term,newvar))
            # assemble atom
            thisatom.append(newvar)
            usedvars.add(newvar)
          else:
            thisatom.append(term)
        if isinstance(thisatom[0], inputformat.Builtin):
          thisout.append(cli(tuple(thisatom)))
        else:
          thisout.append("{}({})".format(forbidWhat, cli(tuple(thisatom))))
      out.append(':- ' + ', '.join(thisout) + '.')
    return out

  def rewriteConstraints(self, constraints):
    return self.rewriteConstraintsHelper(constraints, 'true', map_it = False)

  def rewriteAssumptionConstraints(self, constraints):
    return self.rewriteConstraintsHelper(constraints, 'abduce', map_it = True)

  def rewriteSorts(self, sortnames):
    out = []
    out += map(lambda n: 'sortname({}).'.format(n), sortnames)
    return out

class CTripleRewriterRCRABackchain(CTripleRewriterRCRABase):
  '''
  Rewriter for 'backward chaining' abduction.
  Using c(pred,subj,obj) representation for atom pred(subj,obj).
  '''
  def __init__(self, invent='function'):
    CTripleRewriterRCRABase.__init__(self)
    self.invent = invent

  def rewriteAxioms(self, rules):
    out = []

    for ridx, rule in enumerate(rules):
      # we do not use % ... because this would make line breaks relevant
      # this method has the advantage to put the full rules to the answer set
      # the disadvantage is a large symbol table (more than double size)
      out.append('comment("{}").'.format(str(rule)))

      #
      # give the possibility to infer the head of a rule via that rule
      #
      # for a rule with head 
      #   get(X,foo,Y)
      # it produces
      #   { inferVia(r1,get(X,foo,Y)) } :- infer(get(X,foo,Y)).
      #

      rrep = rRep(ridx)
      hpred = rule.head[0]
      hterms = list(rule.head[1:])

      vmapper = VariableMapper(invent=self.invent)
      vmapper.transform_register(hterms)

      # build string for hterms:
      # if we have no terms, the head is a 0-ary predicate
      hlit = cli([hpred]+hterms)
      out.append('{{ inferVia({rrep},{hlit}) }} :- infer({hlit}).'.format(rrep=rrep, hlit=hlit))

      # if we infer via a certain rule,
      # this adds requirements corresponding to its body
      #
      # for the rule (example is just for syntactic purposes)
      #   get(?x,foo,?y) :- give(?z,?x,?y), obj(foo,?z,?y).
      # it produces
      #   inferenceNeeds(c(get,X0,foo,X1),r1,c(give,skr1_0(X0,X1),X0,X1) :-
      #       inferVia(r1,c(get,X0,foo,X1)).
      #   inferenceNeeds(c(get,X0,foo,X1),r1,c(obj,foo,skr1_0(X0,X1),X1) :-
      #       inferVia(r1,c(get,X0,foo,X1)).
      # where skr1_2 is the skolem function of rule 1 of variable
      # index 2 that is not bound by the head
      for b in rule.body:
        bpred = b[0]
        # prepare terms with variable/skolem replacements
        bterms = list(b[1:])
        extrabody = vmapper.transform_skolemize(bterms)
        assert(not extrabody)
        # build rule
        blit = cli([bpred]+bterms)
        out.append('inferenceNeeds({hlit},{rrep},{blit}) :- inferVia({rrep},{hlit}).'.format(
          rrep=rrep, hlit=hlit, blit=blit))

        if hpred == bpred and isVar(hterms[0]) and isVar(hterms[1]):
          warn("potential loop in domain rule! "+drule)
          raise Exception("will cause infinite grounding")

    return out

class CTripleRewriterRCRABwfw(CTripleRewriterRCRABase):
  '''
  Rewriter for backward chaining marking -> forward chaining abduction.
  Using c(pred,subj,obj) representation for atom pred(subj,obj).
  For inventing values we have several configurations:
  * 'python' use gringo python externals to create smaller unique terms
  * 'function' use function terms to invent values
  '''
  def __init__(self, invent='function'):
    CTripleRewriterRCRABase.__init__(self)
    self.invent = invent

  def rewriteAxioms(self, rules):
    out = []

    for ridx, rule in enumerate(rules):
      # we do not use % ... because this would make line breaks relevant
      # this method has the advantage to put the full rules to the answer set
      # the disadvantage is a large symbol table (more than double size)
      out.append('comment("{}").'.format(str(rule)))
      #warn("rewriting rule "+str(rule))

      #
      # mark possible constants/skolem terms for predicate positions
      #
      # for the rule (example is just for syntactic purposes)
      #   get(?x,?y) :- give(?y,?z), obj(?z,?x).
      # it produces
      #   pot(c(give,Y,s1(X,Y))) :- pot(c(get,X,Y))
      #   pot(c(obj,s1(X,Y),X)) :- pot(c(get,X,Y))
      # where s1(X,Y) is a skolem function introduced for ?z
      #

      rrep = rRep(ridx)

      hpred = rule.head[0]
      hterms = list(rule.head[1:])
      #warn("got hterms "+repr(hterms))
      #hlit = cli([hpred]+hterms)
      #warn("got hlit "+hlit)

      # register head variables
      vmapper = VariableMapper(invent=self.invent)
      vmapper.transform_register(hterms)

      body = 'pot(c({},{},{}))'.format(hpred, hterms[0], hterms[1])

      for b in rule.body:
        # skolemize new variables in body
        bpred = b[0]
        bterms = list(b[1:])
        extrabody = vmapper.transform_skolemize(bterms)
        if extrabody:
          body = body + extrabody
        #warn("got bterms "+repr(bterms))
        #blit = cli([bpred]+bterms)
        #warn("got blit "+blit)

        head = 'pot(c({},{},{}))'.format(bpred, bterms[0], bterms[1])
        drule = '{} :- {}.'.format(head, body)
        #warn("created domain rule "+repr(drule))
        if hpred == bpred and isVar(hterms[0]) and isVar(hterms[1]):
          warn("potential loop in domain rule! "+drule)
          raise Exception("will cause infinite grounding")
        out.append(drule)

      # do usual inference
      # for the rule (example is just for syntactic purposes)
      #   get(?x,?y) :- give(?y,?z), obj(?z,?x).
      # it produces
      #   true(c(get,X,Y)) :- true(c(give,Y,Z)), true(c(obj,Z,X)).
      head = 'true({})'.format(cli(rule.head))
      bodies = map(lambda elem: 'true({})'.format(cli(elem)), rule.body)
      irule = '{} :- {}.'.format(head, ','.join(bodies))
      #warn("created inference rule "+repr(irule))
      out.append(irule)

    return out

########################################
## END of RCRA 2015 experimental code ##
########################################

class ValueInventHelper:
  '''
  usage pattern: construct with appropriate setting, then transform
   * 'python' use gringo python externals to create smaller unique terms
   * 'function' use function terms to invent values
   * 'eatom' use HEX external atom to invent values
   * 'null' does not invent values but puts constant 'null'
  '''

  def __init__(self, invent='function', function_name='s', python_function_name='skolem'):
    assert(invent in ['function', 'python', 'eatom', 'null'])
    self.invent = invent
    self.function_name = function_name
    self.python_function_name = python_function_name

  def do(self, parameters, var):
    '''
    takes parameters to skolem term and output variable
    returns new positive body atom (safe if parameters are safe)
    '''
    assert(isinstance(parameters, list))
    if self.invent == 'eatom':
      # hex external atom for value invention
      return '&invent[{}]({})'.format(','.join(parameters), var)
    if self.invent == 'function':
      # uninterpreted function for value invention
      return '{} = {}({})'.format(
        var, self.function_name, ','.join(parameters))
    elif self.invent == 'python':
      # Gringo external for value invention
      # (slightly higher performance than 'function' method)
      return '{} = @{}({})'.format(
        var, self.python_function_name, ','.join(parameters))
    elif self.invent == 'null':
      return 'null'
    else:
      raise Exception('should never get here')

class VariableTransformer:
  '''
  usage pattern: construct one instance for each rule to process
  '''
  # index to create new skolem terms
  # this index is globally increasing over all rules
  skolemidx = 1

  def __init__(self, invent='function'):
    '''
    '''
    # variable index, to create ASP variables
    # this index is local to instances of the mapper
    # (i.e., local to the currently processed rule)
    self.vidx = 1
    # record non-skolemized terms
    self.nonskolemvars = set()
    # store mapping from variables to skolem terms
    self.mapper = {}
    #warn("VariableMapper initialized with index {}".format(VariableMapper.skolemidx))
    self.inventer = ValueInventHelper(invent)

  def get_non_skolem_vars(self):
    return self.nonskolemvars

  def transform_register(self, terms):
    '''
    register variables
    '''
    #warn("VariableMapper registering terms  {}".format(repr(terms)))
    for idx, term in enumerate(terms):
      if isVar(term):
        self.nonskolemvars.add(term)
    #warn("VariableMapper register result {}".format(repr(terms)))

  def transform_skolemize(self, terms):
    '''
    use mapper to map variables
    skolemize new variables using existing variables
    returns array with additional body elements for value invention
    DOES NOT CHANGE VARIABLES (we need no additional variables)
    '''
    #warn("VariableMapper skolemizing terms {}".format(repr(terms)))
    #warn("  (skolem_args {})".format(skolem_args))
    #warn("  (hterms {})".format(skolem_hterms))
    out = []
    for idx, term in enumerate(terms):
      if isVar(term) and term not in self.nonskolemvars:
        try:
          # first try
          terms[idx] = self.mapper[term]
        except KeyError:
          # if it does not work: register new skolem term
          #warn("VariableMapper skolemizing with index {}".format(VariableMapper.skolemidx))

          # we have at least one term: the index (to make it unique to the rule)
          skolemparameters = list(self.nonskolemvars) + [str(VariableTransformer.skolemidx)]
          out.append(self.inventer.do(skolemparameters, term))
          self.mapper[term] = term
          # again, now it must work!
          terms[idx] = self.mapper[term]

          # globally use this skolem idx
          VariableTransformer.skolemidx += 1
    #warn("VariableTransformer skolemize result {} {}".format(repr(terms), repr(out)))
    return out

def cli(tup):
  'makes a c(.,.,.) style atom out of a tuple/list'
  assert(len(tup) > 0)
  if len(tup) == 1:
    return tup[0]
  else:
    if isinstance(tup[0], inputformat.Builtin):
      return "{arg1} {op} {arg2}".format(op=str(tup[0]), arg1=tup[1], arg2=tup[2])
    else:
      return "c({})".format(','.join(tup))

class CTripleRewriter(object):
  '''
  Rewriter:
   * using c(pred,subj,obj) representation for atom pred(subj,obj).
  '''
  def __init__(self, constraintsMode='none', assumptionConstraintsMode='none'):
    assert(constraintsMode in ['none', 'var', 'eq', 'ondemand'])
    self.constraintsMode = constraintsMode
    assert(assumptionConstraintsMode in ['none', 'var', 'eq', 'ondemand'])
    self.assumptionConstraintsMode = assumptionConstraintsMode

  def rewrite(self, inst, kb):
    out = []
    out += self.rewriteGoal(inst.goal)
    out += self.rewriteAxioms(kb.axioms)
    out += self.rewriteConstraints(kb.constraints)
    out += self.rewriteAssumptionConstraints(kb.assumptionconstraints)
    out += self.rewriteSorts(kb.sortnames)
    return out

  def rewriteGoal(self, goal):
    out = []
    #warn('goal to rewrite: '+repr(goal))
    for b in goal:
      for t in b:
        # for now: assume we have only constants in the goal (that's the case in ACCEL)
        assert(not isVar(t))
      out.append('goal({}).'.format(cli(b)))
    return out

  def rewriteConstraintsHelper(self, constraints, forbidWhat, mode):
    # forbidWhat is the predicate used to forbid (see rewriteConstraints and rewriteAssumptionConstraints)
    # map_it == True -> assume we do not have canonical information in forbidWhat but use map/2
    out = []
    out.append('comment("mode = {}").'.format(mode))
    if mode == 'none':
      return out
    what=forbidWhat
    for constraint in constraints:
      # we do not use % ... because this would make line breaks relevant
      # this method has the advantage to put the full rules to the answer set
      # the disadvantage is a large symbol table (more than double size)
      out.append('comment("{}").'.format(str(constraint)))
      
      # we do not need any skolemized terms here
      # true atoms are marked with true(<atom>)
      #
      # we can handle a limited set of cases with this code
      #
      # UNIQUE-SLOT-constraints:
      #   <- token(E,V1), token(E,V2), V1 != V2
      # are transformed into (mode var)
      #   <- <what>(c(token,E,V1)), <what>(c(token,E,V2)), V1 < V2.
      # are transformed into (mode eq)
      #   <- <what>(c(token,E1,V1)), <what>(c(token,E2,V2)),
      #      eq(E1,E2), V1 < V2, not eq(V1,V2).
      # (we can only break one symmetry!)
      # (eq(X,X) is true for all HU elements, not only UHU!)
      # are transformed into (mode ondemand)
      #   ondemanduniqueslot(<what>,token).
      #
      # ASSUMPTION constraints:
      #   <- go_step(S,G), goer(G,P)
      # are transformed into (mode var)
      #   <- <what>(c(go_step,S,G)), <what>(c(goer,G,P)).
      # are transformed into (mode eq)
      #   <- <what>(c(go_step,S,G1)), <what>(c(goer,G2,P)),
      #      eq(G1,G2).
      # (if we say G1 <= G2 we eliminate too much)
      # (eq(X,X) is true for all HU elements, not only UHU!)
      # are transformed into (mode ondemand)
      #   ondemandassumption(<what>,go_step,s,g,goer,g,p).

      if len(constraint.body) == 3:
        # UNIQUE-SLOT

        # check that 1 and 2 are not builtins and that 3 is !=
        for b in constraint.body[0:2]:
          if isinstance(b[0], inputformat.Builtin):
            raise Exception("TODO: implement builtin for 3-body-constraints")
        if (not isinstance(constraint.body[2][0], inputformat.Builtin)) or constraint.body[2][0].operator != '!=':
          raise Exception("TODO: implement other builtin for 3-body-constraints {}".format(str(constraint)))

        pred = constraint.body[0][0]
        assert(pred == constraint.body[1][0]) # same pred for unique slot
        var = constraint.body[0][1]
        assert(var == constraint.body[1][1]) # same var in first place
        # let's assume the rest is ok

        if mode == 'var':
          out.append(':- {}({}), {}({}), X < Y.'.format(
            what, cli([pred, 'E', 'X']), what, cli([pred, 'E', 'Y'])))
        elif mode == 'eq':
          out.append(':- {w}({a1}), {w}({a2}), eq(E1,E2), X < Y, not eq(X,Y).'.format(
            w=what, a1=cli([pred, 'E1', 'X']), a2=cli([pred, 'E2', 'Y'])))
        elif mode == 'ondemand':
          out.append('ondemanduniqueslot({},{}).'.format(what, pred))
        else:
          raise Exception("unknown mode {}".format(mode))

      elif len(constraint.body) == 2:
        # ASSUMPTION

        # check there are no builtins
        for b in constraint.body:
          if isinstance(b[0], inputformat.Builtin):
            raise Exception("TODO: implement builtin for 2-body-constraints")

        p1, t11, t12 = constraint.body[0]
        p2, t21, t22 = constraint.body[1]
        preds = [p1,p2]
        termis = [t11,t12,t21,t22]

        varusage = collections.defaultdict(lambda: 0)
        nonvar = False
        for v in termis:
          if not isVar(v):
            nonvar = True
          varusage[v] += 1
        if sorted(varusage.values()) != [1, 1, 2]:
          raise Exception("TODO: implement other variable usage than 1, 1, 2 in 2-body-constraints ({})".format(varusage.values()))

        if mode == 'var':
          out.append(':- {}({}), {}({}).'.format(
            what, cli(preds[0:1]+termis[0:2]), what, cli(preds[1:2]+termis[2:4])))
        elif mode == 'eq':
          idx = 1
          special = []
          #warn("varusage {} varis {}".format(repr(varusage), repr(varis)))
          for vidx, v in enumerate(termis):
            if varusage[v] > 1:
              specialvar = 'My{}{}'.format(v,vidx)
              termis[vidx] = specialvar
              special.append(specialvar)
          #warn("varusage {} varis {} special {}".format(repr(varusage), repr(varis), repr(special)))
          out.append(':- {w}({a1}), {w}({a2}), eq({s1},{s2}).'.format(
            w=what, a1=cli(preds[0:1]+termis[0:2]), a2=cli(preds[1:2]+termis[2:4]),
            s1=special[0], s2=special[1]))
        elif mode == 'ondemand':
          if nonvar:
            raise Exception("TODO: implement non-variable term in 2-body-constraints")
          out.append('ondemandassumption({},{},{},{},{},{},{}).'.format(
            what, p1, t11.lower(), t12.lower(), p2, t21.lower(), t22.lower()))
        else:
          raise Exception("unknown mode {}".format(mode))
      else:
        raise Exception("TODO: implement constraints with not in [2,3] bodies")

      ## commented out old code (that was more generic but has no potential for optimization)
      #varusage = collections.defaultdict(lambda: 0)
      #for atom in constraint.body:
      #  for term in atom[1:]:
      #    if isVar(term):
      #      varusage[term] += 1
      ## now we know which variables must be changed (those used more than once)
      ## create output atoms and mappings
      #usedvars = set(varusage.keys())
      #newidx = 0
      #thisout = []
      #for atom in constraint.body:
      #  thisatom = [atom[0]]
      #  for term in atom[1:]:
      #    if term in varusage and varusage[term] > 1 and map_it:
      #      # find new variable
      #      while True:
      #        newvar = 'X{}'.format(newidx)
      #        newidx += 1
      #        if newvar not in usedvars:
      #          break
      #      # output mapping
      #      thisout.append('map({},{})'.format(term,newvar))
      #      # assemble atom
      #      thisatom.append(newvar)
      #      usedvars.add(newvar)
      #    else:
      #      thisatom.append(term)
      #  if isinstance(thisatom[0], inputformat.Builtin):
      #    if thisatom[0].operator == '!=':
      #      # XXX this is 'eq'-encoding-specific!
      #      thisout.append('{a} < {b}, not eq({a},{b})'.format(a=thisatom[1], b=thisatom[2]))
      #    else:
      #      warn("unexpected builtin {} in constraint!".format(repr(thisatom)))
      #      thisout.append(cli(tuple(thisatom)))
      #  else:
      #    thisout.append("{}({})".format(forbidWhat, cli(tuple(thisatom))))
      #out.append(':- ' + ', '.join(thisout) + '.')
    return out

  def rewriteConstraints(self, constraints):
    return self.rewriteConstraintsHelper(constraints, 'true', mode=self.constraintsMode)

  def rewriteAssumptionConstraints(self, constraints):
    return self.rewriteConstraintsHelper(constraints, 'abduce', mode=self.assumptionConstraintsMode)

  def rewriteSorts(self, sortnames):
    out = []
    out += map(lambda n: 'sortname({}).'.format(n), sortnames)
    return out

class CTripleRewriterBackForward(CTripleRewriter):
  '''
  Rewriter for backward chaining marking -> forward chaining abduction.
  Using c(pred,subj,obj) representation for atom pred(subj,obj).
  For inventing values we have several configurations:
  * 'python' use gringo python externals to create smaller unique terms
  * 'function' use function terms to invent values
  '''
  def __init__(self, constraintsMode='none', assumptionConstraintsMode='none', inventTerm='function'):
    CTripleRewriter.__init__(self, constraintsMode, assumptionConstraintsMode)
    assert(inventTerm in ['function', 'python'])
    self.inventTerm = inventTerm

  def rewriteAxioms(self, rules):
    out = []

    for ridx, rule in enumerate(rules):
      # we do not use % ... because this would make line breaks relevant
      # this method has the advantage to put the full rules to the answer set
      # the disadvantage is a large symbol table (more than double size)
      out.append('comment("{}").'.format(str(rule)))
      #warn("rewriting rule "+str(rule))

      #
      # mark possible constants/skolem terms for predicate positions
      #
      # for the rule (example is just for syntactic purposes)
      #   get(?x,?y) :- give(?y,?z), obj(?z,?x).
      # it produces
      #   pot(c(give,Y,s1(X,Y))) :- pot(c(get,X,Y))
      #   pot(c(obj,s1(X,Y),X)) :- pot(c(get,X,Y))
      # where #skolem is managed by ValueInventer
      # and skolemizes unique to this rule and variable (z)
      #

      rrep = rRep(ridx)

      hpred = rule.head[0]
      hterms = list(rule.head[1:])
      #warn("got hterms "+repr(hterms))
      #hlit = cli([hpred]+hterms)
      #warn("got hlit "+hlit)

      # register head variables
      vmapper = VariableTransformer(invent=self.inventTerm)
      vmapper.transform_register(hterms)

      axiom_head_dom = 'pot(c({},{},{}))'.format(hpred, hterms[0], hterms[1])
      body = axiom_head_dom

      # for recursive rules
      extracondition = ''
      for b in rule.body:
        bpred = b[0]
        if hpred == bpred and isVar(hterms[0]) and isVar(hterms[1]):
          extracondition = ', allowrecursiveaxioms'
          break

      for b in rule.body:
        # skolemize new variables in body
        bpred = b[0]
        bterms = list(b[1:])
        extrabody = vmapper.transform_skolemize(bterms)
        if extrabody:
          body = ', '.join([body] + extrabody)
        #warn("got bterms "+repr(bterms))
        #blit = cli([bpred]+bterms)
        #warn("got blit "+blit)

        head = 'pot(c({},{},{}))'.format(bpred, bterms[0], bterms[1])
        drule = '{} :- {}{}.'.format(head, body, extracondition)
        #warn("created domain rule "+repr(drule))
        out.append(drule)

      # do usual inference
      # for the rule (example is just for syntactic purposes)
      #   get(?x,?y) :- give(?y,?z), obj(?z,?x).
      # it produces
      #   true(c(get,X,Y)) :- true(c(give,Y,Z)), true(c(obj,Z,X)).
      # with magicset, it additionally adds dom as constraint
      #   (to avoid irrelevant inferences)
      #   true(c(get,X,Y)) :- true(c(give,Y,Z)), true(c(obj,Z,X)), dom(get,X,Y).
      head = 'infer({})'.format(cli(rule.head))
      bodies = map(lambda elem: 'true({})'.format(cli(elem)), rule.body)
      #if self.magicset:
      #  bodies.append(axiom_head_dom)
      irule = '{} :- {}{}.'.format(head, ','.join(bodies), extracondition)
      #warn("created inference rule "+repr(irule))
      out.append(irule)

    return out

class CTripleRewriterBackchainDetLimited(CTripleRewriter):
  '''
  Difference to CTripleRewriterBackchainDet:
  * represents skolemization in mayInfer
    -> if skolemization is limited we do not infer without parents
  '''
  def __init__(self, constraintsMode='none', assumptionConstraintsMode='none', inventTerm='function'):
    CTripleRewriter.__init__(self, constraintsMode, assumptionConstraintsMode)
    assert(inventTerm in ['function', 'python'])
    self.inventTerm = inventTerm

  def rewriteAxioms(self, rules):
    out = []

    for ridx, rule in enumerate(rules):
      # we do not use % ... because this would make line breaks relevant
      # this method has the advantage to put the full rules to the answer set
      # the disadvantage is a large symbol table (more than double size)
      out.append('comment("{}").'.format(str(rule)))

      # deterministically build the backward chaining tree
      #
      # for the rule r1 (example is just for syntactic purposes)
      #   get(?x,?y) :- give(?z,?x), obj(?y,?z).
      # it produces
      #   mayInferVia(r1,c(get,X,Y),l(#skolem(r1,z,X,Y))) :- pot(c(get,X,Y)).
      #   inferenceNeeds(c(get,X,Y),r1,c(give,Z,X)) :-
      #     mayInferVia(r1,c(get,X,Y),l(Z)).
      #   inferenceNeeds(c(get,X,Y),r1,c(obj,Y,Z)) :-
      #     mayInferVia(r1,c(get,X,Y),l(Z)).
      # where #skolem is managed by ValueInventer
      # and skolemizes unique to this rule and variable (z)
      #
      # if we have no value invention, the l() becomes l

      rrep = rRep(ridx)
      hpred = rule.head[0]
      hterms = list(rule.head[1:])

      vmapper = VariableTransformer(invent=self.inventTerm)
      vmapper.transform_register(hterms)

      # register variable/skolem replacements
      var_skol_reg = {}
      newbodies = []
      for b in rule.body:
        bterms = list(b[1:]) # list() copies the list
        newbodies += vmapper.transform_skolemize(bterms)
        #for origt, newt in zip(b[1:], bterms):
        #  if isVar(origt) and not isVar(newt):
        #    var_skol_reg[origt] = newt
        for t in bterms:
          if isVar(t) and t not in vmapper.get_non_skolem_vars():
            var_skol_reg[t] = t
      # make a sorted list
      var_skol_list = list(var_skol_reg.iteritems())
      if len(var_skol_list) == 0:
        skollist_head = 'l'
        skollist_body = 'l'
      else:
        skollist_head = 'l({})'.format(','.join(
          map(lambda x: x[1], var_skol_list)))
        skollist_body = 'l({})'.format(','.join(
          map(lambda x: x[0], var_skol_list)))

      # for recursive rules
      extracondition = ''
      for b in rule.body:
        bpred = b[0]
        if hpred == bpred and isVar(hterms[0]) and isVar(hterms[1]):
          extracondition = ', allowrecursiveaxioms'
          break

      # extra bodies for skolemization
      extra = extracondition
      if len(newbodies) > 0:
        extra += ', '+', '.join(newbodies)

      # build string for hterms
      # (if we have no terms, the head is a 0-ary predicate)
      hlit = cli([hpred]+hterms)
      out.append('mayInferVia({rrep},{hlit},{skols}) :- pot({hlit}){extra}.'.format(
        rrep=rrep, hlit=hlit, extra=extra, skols=skollist_head))
      out.append('numberOfBodies({rrep},{num}).'.format(rrep=rrep, num=len(rule.body)))

      for b in rule.body:
        bpred = b[0]
        # prepare terms with variable/skolem replacements
        bterms = list(b[1:])
        # build rule
        blit = cli([bpred]+bterms)

        out.append('inferenceNeeds({hlit},{rrep},{blit}) :- mayInferVia({rrep},{hlit},{skols}){extracond}.'.format(
          rrep=rrep, hlit=hlit, blit=blit,
          extracond=extracondition, skols=skollist_body))
    return out

if __name__ == '__main__':
  import accel_input
  p = accel_input.AccelParser()

  ctr_eq_py_bwd = CTripleRewriterBackchainDetLimited(
      constraintsMode='eq', assumptionConstraintsMode='eq', inventTerm='python')

  insts = p.parseInstances('accel_STORY/data-pr-train.lisp')
  for iname, idata in insts.items():
    warn('instance {}'.format(iname))
    warn('\n'.join(ctr_eq_py_bwd.rewriteGoal(idata.goal)))

  warn('KB')
  kb = p.parseKB('accel_STORY/kb-pr.lisp')
  warn('\n'.join(ctr_eq_py_bwd.rewriteAxioms(kb.axioms)))
