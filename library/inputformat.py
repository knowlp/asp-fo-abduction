# First Order Horn Abduction in Answer Set Programming
# Copyright (C) 2015  Peter Schueller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Input Representation for First Order Horn Abduction problems

import sys

def warn(*args):
  sys.stderr.write(' '.join(map(str,args))+'\n')

def hli(tup):
  'makes a human readable atom out of a tuple'
  assert(isinstance(tup,tuple))
  assert(len(tup) > 0)
  if len(tup) == 1:
    return tup[0]
  else:
    if len(tup) == 3 and isinstance(tup[0], Builtin):
      return "{} {} {} ".format(tup[1], tup[0], tup[2])
    else:
      return "{}({})".format(tup[0], ','.join(tup[1:]))

class Instance:
  '''
  one abduction instance (including solution)
  '''
  def __init__(self, name, goal, solution):
    self.name = name
    for g in goal:
      assert(isinstance(g, tuple))
      assert(len(g) == 3)
    self.goal = goal
    for s in solution:
      assert(isinstance(g, tuple))
      assert(len(g) == 3)
    self.solution = solution

  def __str__(self):
    return ('Instance {}:\n  '.format(self.name) +
      '\n  '.join(map(lambda x: hli(x), self.goal)) +
      '\n  Solution:\n    ' +
      '\n    '.join(map(lambda x: hli(x), self.solution)))

class Builtin:
  def __init__(self, operator):
    assert(operator in ['<', '=', '!=', '>'])
    self.operator = operator
  def __str__(self):
    return self.operator

class RuleBase:
  '''
  head = None or atom
  body = None or nonempty list of atoms
  atom = tuple of strings or
         tuple where first is an instance of Builtin
  '''
  def __init__(self, head, body):
    self.head = None
    if head:
      assert(isinstance(head, tuple))
      assert(len(head) == 3)
      self.head = head
    self.body = None
    if body:
      assert(isinstance(body,list))
      if len(body) > 0:
        for b in body:
          assert(isinstance(b, tuple))
          assert(len(b) == 3)
        self.body = body

  def __str__(self):
    disp = []
    if self.head:
      disp.append(hli(self.head))
    disp.append('<-')
    if self.body:
      disp.append(', '.join(map(hli, self.body)))
    return ' '.join(disp)

class Rule(RuleBase):
  '''
  head = atom
  body = None or nonempty list of atoms
  atom = tuple of strings or
         tuple where first is an instance of Builtin
  '''
  def __init__(self, head, body):
    assert(isinstance(head,tuple))
    RuleBase.__init__(self, head, body)

class Constraint(RuleBase):
  '''
  no head
  body = nonempty list of atoms
  atom = tuple of strings or
         tuple where first is an instance of Builtin
  '''
  def __init__(self, body):
    assert(isinstance(body,list))
    assert(len(body) > 0)
    RuleBase.__init__(self, None, body)

class KB:
  def __init__(self):
    self.axioms = []
    self.constraints = []
    self.assumptionconstraints = []
    self.plannames = set()
    self.sortnames = set()

  def __str__(self):
    return ('axioms\n' +
      '\n'.join(map(str, self.axioms)) +
      '\nconstraints\n' +
      '\n'.join(map(str, self.constraints)) +
      '\nassumptionconstraints\n' +
      '\n'.join(map(str, self.assumptionconstraints)) +
      '\nplannames\n' + repr(self.plannames) +
      '\nsortnames\n' + repr(self.sortnames))

# Running example published in RCRA Workshop
def demoInstKbRCRA(root):
  insts = {}
  goal = [
    ('name', 'm', 'mary'), ('lost', 'm', 'f'), ('father_of', 'f', 'm'),
    ('inst', 's', 'female'), ('is', 's', 'depressed')]
  solution = []
  insts['mlf'] = inputformat.Instance('mlf', goal, solution)
  p = accel_input.AccelParser()
  kb = p.parseKBFromString('''
(setf *brules*
  '(
    (<- (inst ?x male) (father-of ?x ?y))
    (<- (inst ?x female) (name ?x mary))
    (<- (is ?x depressed) (inst ?x pessimist))
    (<- (is ?x depressed) (important-for ?y ?x) (is ?y dead))
    (<- (important-for ?y ?x) (father-of ?y ?x))
    (<- (lost ?x ?y) (inst ?y person) (important-for ?y ?x) (is ?y dead))
   ))

(setf *nogoods*
  '(
    ((inst ?x male) (inst ?x female))
    ((father-of ?x ?y) (inst ?x female))
   ))

(setf *sort-hierarchy*
  '(
    (person . (male female))
    (state . (dead depressed))
   ))

(setf *unique-slot-value-predicates*
  '(go-step goer dest-go source-go vehicle
   ))
  ''')
  return insts, kb
