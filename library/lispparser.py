# First Order Horn Abduction in Answer Set Programming
# Copyright (C) 2015  Peter Schueller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# Very simple Lisp parser, just good enough to handle ACCEL KB and instances.
#
# usage:
#
# import lispparser
#
# structure = lispparser.parse(text)
#

import ply.lex as lex
import ply.yacc as yacc

tokens = (
  'PRIME', 'LPAR', 'RPAR',
  'IDENT', 'STARRED', 'COLONED', 'VAR',
  'STRING', 'NUMBER', 'OPERATOR')

t_ignore = ' \t\r\n'
t_ignore_comment = r';.*'

t_PRIME = r"'"
t_LPAR = r'\('
t_RPAR = r'\)'
t_IDENT = r'[a-z#][a-zA-Z0-9-_?]*'
t_STARRED = r'\*[a-zA-Z0-9-_]+\*'
t_COLONED = r':[a-zA-Z0-9-_]+'
t_VAR = r'\?[a-z][a-zA-Z0-9-_]*'
t_STRING = r'"[^"]*"'
# this is incomplete but enough for our current purpose
t_OPERATOR = r'[+\-*/\.]|<-' 


def t_NUMBER(t):
  r'[1-9][0-9]*'
  t.value = int(t.value)
  return t

def t_error(t):
  print "illegal character '{}'".format(repr(t))
  t.lexer.skip(1)

mylexer = lex.lex()

# first rule = start symbol
def p_file(p):
  '''file : expr file
          | expr'''
  if len(p) == 2:
    p[0] = [p[1]]
  else:
    p[0] = [p[1]] + p[2]

def p_exprlist(p):
  '''exprlist : expr
              | expr exprlist'''
  if len(p) == 2:
    p[0] = [p[1]]
  else:
    p[0] = [p[1]] + p[2]

def prime(content):
  return ("'",content)

def p_expr(p):
  '''expr : LPAR exprlist RPAR
          | PRIME expr
          | IDENT
          | STARRED
          | COLONED
          | VAR
          | STRING
          | NUMBER
          | OPERATOR'''
  if len(p) == 2:
    p[0] = p[1]
  elif len(p) == 3:
    p[0] = prime(p[2])
  else:
    p[0] = p[2]

def p_error(p):
  print "TODO unexpected something '{}'".format(repr(p))

myparser = yacc.yacc()

def parse(content):
  return myparser.parse(content, lexer=mylexer)
