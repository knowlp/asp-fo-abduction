#!/usr/bin/python

# script for calling Phillip
# https://github.com/kazeto/phillip

ILPDIR='/home/ps/_setup/phillip'
LPSOLVE_PATH='/home/ps/_setup/lp_solve_5.5/lpsolve55/bin/ux64/'

import subprocess
import sys
import tempfile
import os
import signal

SIGTERM_SENT = False
def sigterm_handler(signum, frame):
  print >>sys.stderr, "SIGTERM handler.  Shutting Down."
  global SIGTERM_SENT
  if not SIGTERM_SENT:
    SIGTERM_SENT = True
    mypg = os.getpgrp()
    print >>sys.stderr, "Sending TERM to PG "+str(mypg)
    os.killpg(mypg, signal.SIGTERM)
  sys.exit()

# set session ID to this process so we can kill group in sigterm handler
os.setsid()
# propagate from my killing to group killing
signal.signal(signal.SIGTERM, sigterm_handler)

COMPILEDPREFIX=sys.argv[1]
KB=sys.argv[2]
INST=sys.argv[3]
ARGS=sys.argv[4:]

env = dict(os.environ)
if 'LD_LIBRARY_PATH' in env:
  env['LD_LIBRARY_PATH'] += ':'+ LPSOLVE_PATH
else:
  env['LD_LIBRARY_PATH'] = LPSOLVE_PATH

# compile KB
cmd='{ilpdir}/bin/phil -m compile_kb -c dist=basic -c tab=null -k {prefix} {kb}'.format(
  ilpdir=ILPDIR, prefix=COMPILEDPREFIX, kb=KB)
subprocess.call(cmd, shell=True, env=env)

# infer
cmd='{ilpdir}/bin/phil -m infer -k {prefix} -c lhs=a* -c ilp=weighted -p max_depth=2 -p default_obs_cost=100.0 -c sol=lpsolve -H {args} {inst}'.format(
  ilpdir=ILPDIR, prefix=COMPILEDPREFIX, inst=INST, args=' '.join(ARGS))
subprocess.call(cmd, shell=True, env=env)

