
# Contents

* Encodings, scripts, and instances, of Clingo/WASP experiments presented in [1].
  (original state in tag `fundamenta-informaticae-rcra-si`)

* Encodings, plugin, and instances, of WASP 2.0 propagator experiments presented in [2].
  (original state in tag `rcra2016-wasp-prop`)

[1] Peter Schüller.
    Modeling Variations of First-Order Horn
    Abduction in Answer Set Programming.
    Fundamenta Informaticae, 2016.
    In press, available as arXiv:1512.08899 [cs.AI].

[2] Francesco Ricca, Carmine Dodaro, and Peter Schüller.
    External Propagators in WASP: Preliminary Report.
    International Workshop on Experimental Evaluation of
    Algorithms for Solving Problems with Combinatorial
    Explosion (RCRA), 2016. To appear.

# Detailed Contents

* `encodings/instances/`

  ACCEL benchmark instances rewritten to ASP facts.

* `encodings/kb/`

  ACCEL knowledge base rewritten to ASP rules
  (several rewriting variations), see [1]

* `encodings/modules/`

  ASP program modules for realizing variations of
  abduction, including objective functions `CARD`,
  `COH`, `WA`, see [1].

* `create_full_encodings.py`

  Script to assemble the modular parts of encodings
  into complete encodings (including KB) that can
  be combined with instances to reproduce results of
  the computational experiments.

  Results are written to directory `encodings/full/`

* `engine/run.py`

  Algorithms for evaluating on-demand-constraints + optimization
  as described in [1].

* `engine/wasp-plugin.py`

  Plugin for WASP 2.0 (plugins branch from 2016-09-02), see [2].

* `library/`

  Other python libraries for parsing ACCEL and performing the rewriting
  described in [1].

# Usage Instructions for reproducing [1]

  Generate all encodings:

    $ python create_full_encodings.py

  Run one of the `fi-` encodings with one instance using Python engine:

    $ python engine/run.py <encoding> <instance> <clingo-arguments>

  For example

    $ python engine/run.py encodings/full/fi-table1-pure-asp-bwd-a-wa.lp encodings/instances/17.lp --configuration=crafty

  The output on STDOUT is in JSON format.

  Run encoding with Gringo and Clasp or WASP:

    $ gringo <encoding> <instance> | clasp

    $ gringo <encoding> <instance> | wasp

  The following `fi-` encodings can be evaluated with Gringo and Clasp/WASP: `table1`, `table2-*-uf-*`, `table3-*-rules-*`
  All other `fi-` encodings require on-demand constraints (run.py).

# Usage Instructions for reproducing [2]

  Generate all encodings:

    $ python create_full_encodings.py

  Run an encoding with Gringo and WASP 2.0 with Python plugin:

    $ gringo <encoding> <instance> | wasp --plugins-interpreter=python --plugins-files=engine/wasp-plugin.py

  For example:

    $ gringo encodings/full/rcra16-ondemand-bwd-a-wa.lp encodings/instances/17.lp | wasp --plugins-interpreter=python --script-directory=engine --plugins-files=wasp-plugin

  Run an encoding just with Gringo and Clasp/WASP:

    $ gringo <encoding> <instance> | clasp

    $ gringo <encoding> <instance> | wasp

  The `rcra16-rules-*` encodings can be evaluated with Gringo and Clasp/WASP, the `rcra2016-ondemand-*` encodings require the WASP plugin.

# License

  First Order Horn Abduction in Answer Set Programming
  Copyright (C) 2015, 2016  Peter Schueller <schueller.p@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

